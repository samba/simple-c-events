
DEBUG ?= 0
COMPILE_FLAGS=-Wall
DEBUG_FLAGS=-ggdb -D DEBUG=$(DEBUG)

.PHONY: all clean test debug

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Darwin)
    COMPILE_FLAGS += "-stdlib=libstdc++"
endif


all: build/testing.exe

gdb: build/testing.exe
	gdb build/testing.exe

test: build/testing.exe
	build/testing.exe

build/testing.exe: src/test.c build/libeventqueue.o 
	mkdir -p `dirname $@`
	gcc $(COMPILE_FLAGS) $(DEBUG_FLAGS) -o $@ $^

build/libeventqueue.o: src/events.c src/events.h
	mkdir -p `dirname $@`
	gcc $(COMPILE_FLAGS) $(DEBUG_FLAGS) -o $@ -c $<



clean:
	@rm -rvf build/ *.exe *.o
