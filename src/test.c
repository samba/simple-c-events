#include "events.h"
#include <string.h>
#include <stdio.h>


void myEventHandler(void* userdata){
  printf("I got called: %s\n", (char*) userdata);
}

// These will be have as unsigned int, compatible with addEvent, triggerEvent, etc.
typedef enum EVENTSTATES {
  EVENT_OPEN = 0,
  EVENT_READ,
  EVENT_CLOSE
} EVENTSTATE_T;


int main(int argc, char** argv){

  EventQueue_t* queue = createEventQueue(3, 15);
 
  // Register an event to the "READ" (second phase) event stack
  addEvent(queue, EVENT_READ, (EventCallback) myEventHandler);

  triggerEvent(queue, EVENT_OPEN, "OPEN");
  triggerEvent(queue, EVENT_READ, "READ"); // this one will print
  triggerEvent(queue, EVENT_CLOSE, "CLOSE");

  freeEventQueue(queue);   


  return 0;
}


